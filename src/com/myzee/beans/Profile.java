package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Profile {
	/* For @Qualifier to work : 
	 * change in the xml file required at <beans ...> tag. add xml schemas and 
	 * add <?xml version = "1.0" encoding = "UTF-8"?> at starting of spring.xml file.
	 */
	
	@Autowired
	@Qualifier(value="student1")		
	private Student student;
	
	public Profile() {
		// TODO Auto-generated constructor stub
	}
	
	public void printStudDat() {
		System.out.println(student.getName() + " : " + student.getAge());
	}
	
}
